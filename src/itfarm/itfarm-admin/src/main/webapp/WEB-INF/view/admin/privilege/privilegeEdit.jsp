<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<script type="text/javascript">
    $(function() {

        $('#pid').combotree({
            url : '${path }/privilege/allTree.do',
            parentField : 'pid',
            lines : true,
            panelHeight : 'auto',
            value : '${privilege.pid}'
        });
        
        $('#resourceEditForm').form({
            url : '${path }/privilege/save.do',
            onSubmit : function() {
                progressLoad();
                var isValid = $(this).form('validate');
                if (!isValid) {
                    progressClose();
                }
                return isValid;
            },
            success : function(result) {
                progressClose();
                result = $.parseJSON(result);
                if (result.success) {
                    parent.$.modalDialog.openner_treeGrid.treegrid('reload');//之所以能在这里调用到parent.$.modalDialog.openner_treeGrid这个对象，是因为resource.jsp页面预定义好了
                    /*parent.layout_west_tree.tree('reload');*/
                    parent.$.modalDialog.handler.dialog('close');
                }
            }
        });

        $("#status").val('${resource.status}');
        $("#resourcetype").val('${privilege.resourcetype}');
    });
</script>
<div style="padding: 3px;">
    <form id="resourceEditForm" method="post">
        <input type="hidden" name="recordId" value="${privilege.recordId}">
        <table  class="grid">
            <tr>
                <td>资源名称</td>
                <td>
                <input name="privilegeName" type="text" placeholder="请输入资源名称" value="${privilege.privilegeName}" class="easyui-validatebox span2" data-options="required:true" ></td>
                <td>资源类型</td>
                <td><select id="resourcetype" name="resourcetype" class="easyui-combobox" data-options="width:140,height:29,editable:false,panelHeight:'auto'">
                            <option <c:if test="${privilege.resourcetype == 0}">selected="selected"</c:if> value="0">菜单</option>
                            <option <c:if test="${privilege.resourcetype == 1}">selected="selected"</c:if> value="1">按钮</option>
                </select></td>
            </tr>
            <tr>
                <td>资源路径</td>
                <td><input name="url" type="text" value="${privilege.url}" placeholder="请输入资源路径" class="easyui-validatebox span2" ></td>
                <td>权限码</td>
                <td><input name="privilegeCode" type="text" placeholder="请输入权限码" value="${privilege.privilegeCode}" class="easyui-validatebox span2" data-options="required:true" ></td>
            </tr>
            <tr>
                <td>菜单图标</td>
                <td ><input  name="iconCls" value="${privilege.iconCls}"/></td>
                <td>状态</td>
                <td ><select id="status" name="status" class="easyui-combobox" data-options="width:140,height:29,editable:false,panelHeight:'auto'">
                            <option value="0">正常</option>
                            <option value="1">停用</option>
                </select></td>
            </tr>
            <tr>
                <td>上级资源</td>
                <td colspan="3"><select id="pid" name="pid" style="width: 200px; height: 29px;"></select>
                <a class="easyui-linkbutton" href="javascript:void(0)" onclick="$('#pid').combotree('clear');" >清空</a></td>
            </tr>
        </table>
    </form>
</div>
