package com.tc.itfarm.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.google.common.collect.Lists;
import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.api.util.DateUtils;
import com.tc.itfarm.model.PrivilegeCriteria;
import com.tc.itfarm.model.RolePrivilege;
import com.tc.itfarm.model.ext.PrivilegeTree;
import com.tc.itfarm.model.ext.Tree;
import com.tc.itfarm.service.RolePrivilegeService;
import com.tc.itfarm.service.RoleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.tc.itfarm.dao.PrivilegeDao;
import com.tc.itfarm.model.Privilege;
import com.tc.itfarm.service.PrivilegeService;
import org.springframework.util.Assert;

@Service
public class PrivilegeServiceImpl extends BaseServiceImpl<Privilege> implements PrivilegeService {

	@Resource
	private PrivilegeDao privilegeDao;
	@Resource
	private RolePrivilegeService rolePrivilegeService;

	@Override
	public Integer save(Privilege privilege) {
		Assert.notNull(privilege, "对象不能为空!");
		Assert.isTrue(StringUtils.isNotBlank(privilege.getPrivilegeCode()), "权限码不能为空!");
		Assert.isTrue(StringUtils.isNotBlank(privilege.getPrivilegeName()), "权限名称不能为空!");
		privilege.setModifyTime(DateUtils.now());
		if (privilege.getRecordId() == null) {
			privilege.setCreateTime(DateUtils.now());
			privilegeDao.insert(privilege);
		}
		return privilegeDao.updateByIdSelective(privilege);
	}

	@Override
	public List<Privilege> selectByRoleId(List<Integer> roleIds) {
		return null;
	}

	@Override
	public List<Privilege> selectByRoleId(Integer roleId) {
		List<RolePrivilege> rolePrivileges = rolePrivilegeService.selectByRoleId(roleId);
		List<Integer> ids = Lists.newArrayList();
		for (RolePrivilege rp : rolePrivileges) {
			ids.add(rp.getPrivilegeId());
		}
		if (ids.size() < 1) {
			return Lists.newArrayList();
		}
		PrivilegeCriteria criteria = new PrivilegeCriteria();
		criteria.or().andRecordIdIn(ids);
		return privilegeDao.selectByCriteria(criteria);
	}

	@Override
	public PageList<Privilege> selectByPageList(String name, String code, Date startDate, Date endDate, Page page) {
		return null;
	}

	@Override
	public List<PrivilegeTree> selectPrivilegetree() {
		List<Privilege> privileges = this.selectTop();
		List<PrivilegeTree> tree = Lists.newArrayList();
		for (Privilege p : privileges) {
			PrivilegeTree pt = new PrivilegeTree();
			setChildren(pt, p);
			tree.add(pt);
		}
		return tree;
	}

	@Override
	public List<Tree> selectAllTree() {
		List<Privilege> privileges = this.selectTop();
		List<Tree> tree = Lists.newArrayList();
		for (Privilege p : privileges) {
			Tree t = new Tree();
			setTreeChildren(t, p);
			tree.add(t);
		}
		return tree;
	}

	private void setTreeChildren(Tree tree, Privilege privilege) {
		// 设置权限
		tree.setId(privilege.getRecordId());
		tree.setIconCls(privilege.getIconCls());
		tree.setPid(privilege.getPid());
		tree.setText(privilege.getPrivilegeName());
		Integer pid = privilege.getRecordId();
		if (pid != null) {
			// 查询所有子权限
			PrivilegeCriteria criteria = new PrivilegeCriteria();
			criteria.or().andPidEqualTo(pid);
			List<Privilege> privileges = privilegeDao.selectByCriteria(criteria);
			// 设置子权限
			List<Tree> children = Lists.newArrayList();
			tree.setChildren(children);
			// 遍历子权限
			for (Privilege p : privileges) {
				Tree t = new Tree();
				setTreeChildren(t, p);
				children.add(t);
			}
		}
	}

	private List<Privilege> selectTop() {
		PrivilegeCriteria criteria = new PrivilegeCriteria();
		criteria.or().andPidIsNull();
		return privilegeDao.selectByCriteria(criteria);
	}

	private void setChildren(PrivilegeTree pt, Privilege privilege) {
		// 设置权限
		pt.setRecordId(privilege.getRecordId());
		pt.setUrl(privilege.getUrl());
		pt.setResourcetype(privilege.getResourcetype());
		pt.setCreateTime(privilege.getCreateTime());
		pt.setModifyTime(privilege.getModifyTime());
		pt.setIconCls(privilege.getIconCls());
		pt.setPid(privilege.getPid());
		pt.setPrivilegeCode(privilege.getPrivilegeCode());
		pt.setPrivilegeName(privilege.getPrivilegeName());

		Integer pid = privilege.getRecordId();
		if (pid != null) {
			// 查询所有子权限
			PrivilegeCriteria criteria = new PrivilegeCriteria();
			criteria.or().andPidEqualTo(pid);
			List<Privilege> privileges = privilegeDao.selectByCriteria(criteria);
			// 设置子权限
			List<PrivilegeTree> children = Lists.newArrayList();
			pt.setChildren(children);
			// 遍历子权限
			for (Privilege p : privileges) {
				PrivilegeTree privilegeTree = new PrivilegeTree();
				setChildren(privilegeTree, p);
				children.add(privilegeTree);
			}
		}
	}

	@Override
	protected SingleTableDao getSingleDao() {
		return privilegeDao;
	}
}
